debug = require("debug")("prometheus-client:registry")
# Removing express dependencies - David Katz
module.exports = class Registry
    constructor: ->
        @_metrics = {}

        @metricsFunc = () =>

            debug "Preparing to write #{ Object.keys(@_metrics).length } metrics."
            retval = ""
            for k,obj of @_metrics
                retval+= """
                # HELP #{k} #{obj.help}
                # TYPE #{k} #{obj.type()}

                """

                for v in obj.values()
                    labels = ("#{lk}=\"#{lv}\"" for lk,lv of v[0]).join(",")
                    retval+= "#{k}{#{labels}} #{v[1]}\n"

            return retval

    #----------

    register: (metric) ->
        # validate that our metric name is unique
        name = metric._full_name
        if @_metrics[ name ]
            throw "Metric name must be unique."

        debug "Registering new metric: #{ name }"
        @_metrics[ name ] = metric

        metric
