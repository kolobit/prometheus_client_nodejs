# Prometheus Client

Cloned from the original source: https://github.com/StreamMachine/prometheus_client_nodejs

WIP [Prometheus](http://prometheus.io) instrumentation metrics library for
Node.JS. Metrics are intended to be scraped by a Prometheus server.

## Usage

### Getting Started

Install the `prometheus-client` package with NPM:

    npm install prometheus-lite

Then require the package and set up a new client instance:

    var Prometheus = require("prometheus-client");

    var client = new Prometheus();
    var metrics = client.metricsFunc();


By default, the Prometheus client will use a global namespace. That means that
any metrics registered inside your app (even by libraries) will show up in your
client without any need to pass around a client object.

See [examples/test.coffee](examples/test.coffee) for a full example of registering and using metrics in coffeescript and [examples/counterAndGauge.js](examples/counterAndGauge.js) to see the same example written in Javascript.